﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DataPlus.Principal
{
    public class Cliente
    {
        private string SQL;

        //public Cliente(TextBox txtNome, MaskedTextBox mtbCpf)
        //{
        //    Nome = txtNome.Text;
        //    CPF = TrataCpf(mtbCpf.Text);
        //    try
        //    {
        //        Atualizar();
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //        throw;
        //    }
        //}
        private string cpf;
        private string phone;



        public string Name { get; set; }

        public string CPF
        {
            get
            {
                return cpf = TrataCpf(cpf);
            }
            set
            {
                cpf = value;
            }
        }
        public string Phone
        {
            get
            {
                return phone = TrataTel(phone);
            }
            set
            {

                phone = value;
            }
        }



        public bool Atualizar()
        {
            SQL = "INSERT INTO CLIENTE (NAME, CPF, PHONE) VALUES('" + Name + "','" + cpf + "','" + phone + "')";
            Conexao conn = new Conexao();
            SqlCommand sqlcmd = new SqlCommand(SQL, conn.conectar());
            try
            {
                sqlcmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Atualizar", MessageBoxButtons.OK,MessageBoxIcon.Error);
                return false;
                throw;
            }
            finally
            {
                conn.desconectar();
            }
            return true;
        }
        public string TrataCpf(string cpf)
        {
            cpf = cpf.Replace(".", "");
            cpf = cpf.Replace("-", "");
            return cpf;
        }

        public string TrataTel(string tel)
        {
            tel = tel.Replace("-", "");
            tel = tel.Replace("(", "");
            tel = tel.Replace(")", "");
            tel = tel.Replace(" ", "");

            return tel;
        }
    }
}
