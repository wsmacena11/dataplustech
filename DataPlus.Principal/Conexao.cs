﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataPlus.Principal
{
    public class Conexao
    {
        private bool failConn;
        SqlConnection conn = new SqlConnection();

        public Conexao()
        {
            if (failConn)
            {
                conn.ConnectionString = @"Data Source=WSM-PC\SQLDEV;Initial Catalog=Enterprise;User Id=sa;Password=inter#system";
            }
            conn.ConnectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=Empresa;Integrated Security=True";
            
        }

        public SqlConnection conectar()
        {
            if (conn.State == System.Data.ConnectionState.Closed)
                try
                {
                    conn.Open();
                }
                catch (SqlException e)
                {
                    MessageBox.Show(e.Message);
                    failConn = true;
                    conn.Close();
                    conn.Open();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.StackTrace);
                }
                finally
                {

                }
                
            return conn;
        }

        public SqlConnection desconectar()
        {
            if (conn.State == System.Data.ConnectionState.Open)
                conn.Close();
            return conn;
        }
        

    }
}
