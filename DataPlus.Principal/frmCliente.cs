﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataPlus.Principal
{
    public partial class frmCliente : Form
    {
        public frmCliente()
        {
            InitializeComponent();
        }

        private void cmdIncluir_Click(object sender, EventArgs e)
        {
            gpbCadastro.Visible = true;
            txtNome.Focus();
        }

        private void cmdConfirmar_Click(object sender, EventArgs e)
        {
            if (CheckData())
            {
                SendData();
                LimparTela();
                gpbCadastro.Visible = false;
            }
        }
        private void LimparTela()
        {
            txtNome.Text = "";
            mtbCpf.Text = "";
            mtbPhone.Text = "";
        }

        private bool SendData()
        {
            Cliente cli = new Cliente();
            cli.Name = txtNome.Text;
            cli.CPF = mtbCpf.Text;
            cli.Phone = mtbPhone.Text;
            if (!cli.Atualizar())
            {
                MessageBox.Show("Error!");
                return false;
            }
            else
            {
                MessageBox.Show(Msg(1),"Atualizar", MessageBoxButtons.OK,MessageBoxIcon.Information);
                return true;
            }
        }

        private void cmdCancelar_Click(object sender, EventArgs e)
        {
            gpbCadastro.Visible = false;
            LimparTela();
        }

        private string Msg(int code)
        {
            string[] Arr = new string[5];

            Arr[0] = "information required";
            Arr[1] = "Successful registration";
            Arr[2] = "Necessário o preenchimento do campo";
            Arr[3] = "Necessário o preenchimento do campo";
            Arr[4] = "Necessário o preenchimento do campo";

            return Arr[code];
        }

        private bool CheckData()
        {
            if (txtNome.Text == "")
            {
                MessageBox.Show(Msg(0));
                txtNome.Focus();
                return false;
            }
            return true;
        }
    }
}
